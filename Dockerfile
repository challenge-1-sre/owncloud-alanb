FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive 
 
RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y apache2 libapache2-mod-php php-gd php-json php-mysql php-sqlite3 php-curl php-intl php-imagick php-zip php-xml php-mbstring php-soap php-ldap php-apcu php-redis php-dev libsmbclient-dev php-gmp smbclient

RUN echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/server:/10/Ubuntu_20.04/ /' | tee /etc/apt/sources.list.d/isv:ownCloud:server:10.list
RUN curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:server:10/Ubuntu_20.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/isv_ownCloud_server_10.gpg > /dev/null

RUN apt-get update && apt-get upgrade -y

RUN apt install owncloud-complete-files

RUN mv /var/www/owncloud /var/www/html

RUN usermod -aG www-data www-data

RUN rm -rf /var/lib/apt/lists/*

EXPOSE 80

ENTRYPOINT [ "/usr/sbin/apache2" ]
CMD [ "-D", "FOREGROUND" ]
